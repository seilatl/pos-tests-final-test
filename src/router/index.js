import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../components/Login.vue';
import User from '../components/User.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/userForm',
    name: 'UserForm',
    props: true,
    component: User,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
