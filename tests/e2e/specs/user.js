describe('User', () => {
  describe('When visit user form url', () => {
    const validEmail = 'email_1@gmail.com';
    const validAge = 55;
    const validOperationSystem = 'Windows';

    beforeEach(() => {
      cy.visit('/userForm');
    });

    describe('When fill name and lastname', () => {
      beforeEach(() => {
        cy.get('.form__input_email').type(validEmail);
        cy.get('.form__input_age').type(validAge);
        cy.get('select').select(validOperationSystem);
      });

      it('should has button', () => {
        cy.get('button').should('be.visible');
      });

      describe('When click button', () => {
        it('should show a success message', () => {
          cy.url().should('eq', 'http://localhost:8080/userForm'); // => true
          cy.get('button').click();
          cy.contains('p', 'Parabéns! Formulário preenchido com sucesso!');
        });
      });
    });
  });
});
