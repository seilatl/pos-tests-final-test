describe('Login', () => {
  describe('When visit login url', () => {
    const validEmail = 'email_1@gmail.com';
    const validPassword = 'passord1';

    beforeEach(() => {
      cy.visit('/');
    });

    describe('When fill name and lastname', () => {
      beforeEach(() => {
        cy.get('.form__input_email').type(validEmail);
        cy.get('.form__input_password').type(validPassword);
      });

      it('should has button', () => {
        cy.get('button').should('be.visible');
      });

      describe('When click button', () => {
        it('should go to user form', () => {
          cy.url().should('eq', 'http://localhost:8080/'); // => true
          cy.get('button').click();
          cy.url().should('eq', 'http://localhost:8080/userForm'); // => true
        });
      });
    });
  });
});
