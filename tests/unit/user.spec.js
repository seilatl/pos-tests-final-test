import { shallowMount } from '@vue/test-utils';

import User from '@/components/User.vue';

describe('UserForm', () => {
  it('is a valid component', () => {
    const wrapper = shallowMount(User);
    expect(wrapper.exists()).toBe(true);
  });

  describe('select', () => {
    describe('if exists 3 options', () => {
      it('should be true', () => {
        const wrapper = shallowMount(User);
        const options = wrapper.find('select').findAll('option');
        expect(options).toHaveLength(3);
      });
    });
  });

  describe('When age exists', () => {
    it('should be false when age is between 18 and 99', () => {
      const wrapper = shallowMount(User);

      expect(wrapper.vm.isValidAge(25)).toBe(true);
    });

    it('should be false when age is not between 18 and 99', () => {
      const wrapper = shallowMount(User);

      expect(wrapper.vm.isValidAge(11)).toBe(false);
      expect(wrapper.vm.isValidAge(100)).toBe(false);
    });
  });

  describe('Validations', () => {
    describe('isValidEmail', () => {
      describe('When email is valid', () => {
        it('should return true', () => {
          const wrapper = shallowMount(User);

          expect(wrapper.vm.isValidEmail('teste@teste.com')).toBe(true);
        });
      });
      describe('When email is NOT valid', () => {
        it('should return false', () => {
          const wrapper = shallowMount(User);

          expect(wrapper.vm.isValidEmail('teste')).toBe(false);
        });
      });
    });

    describe('isValidAge', () => {
      describe('When age is valid', () => {
        it('should return true', () => {
          const wrapper = shallowMount(User);

          expect(wrapper.vm.isValidAge(55)).toBe(true);
        });
      });
      describe('When age is NOT valid', () => {
        it('should return false', () => {
          const wrapper = shallowMount(User);

          expect(wrapper.vm.isValidAge(5)).toBe(false);
          expect(wrapper.vm.isValidAge(102)).toBe(false);
        });
      });
    });

    describe('Error messages', () => {
      describe('When age is not between 18 and 99', () => {
        it('should show an error message', () => {
          const wrapper = shallowMount(User, {
            data: () => ({ age: 5 }),
          });
          expect(wrapper.find('.form__age_error').text()).toBe('Idade deve ser entre 18 e 99 anos');
          expect(wrapper.find('.form__age_error').attributes('style')).not.toBe('display: none;');
        });
      });
    });
  });

  describe('When all fields are correctly filled', () => {
    it('should show a success message', async () => {
      const wrapper = shallowMount(User, {
        data: () => ({ email: '', age: 55, operational_system: 'Windows' }),
        propsData: {
          email_user: 'email_100@gmail.com',
        },
      });

      wrapper.find('.form__button_validate_form').trigger('click');

      await wrapper.vm.$forceUpdate();

      expect(wrapper.find('.form__success').text()).toBe('Parabéns! Formulário preenchido com sucesso!');
      expect(wrapper.find('.form__success').attributes('style')).not.toBe('display: none;');
    });
  });
});
