import { shallowMount } from '@vue/test-utils';

import Login from '@/components/Login.vue';

describe('Login', () => {
  it('is a valid component', () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.exists()).toBe(true);
  });

  describe('computed', () => {
    describe('isFormCompleted', () => {
      describe('When has email and password', () => {
        describe('And email and password are valid', () => {
          it('should be true', () => {
            const wrapper = shallowMount(Login, {
              data: () => ({ email: 'teste@teste.com', password: '123456' }),
            });
            expect(wrapper.vm.isFormCompleted).toBe(true);
          });
        });

        describe('And email and password are NOT valid', () => {
          it('should be false When email is NOT valid', () => {
            const wrapper = shallowMount(Login, {
              data: () => ({ email: 'e', password: '123456' }),
            });
            expect(wrapper.vm.isFormCompleted).toBe(false);
          });

          it('should be false When password length is lesser than 6', () => {
            const wrapper = shallowMount(Login, {
              data: () => ({ email: 'teste@teste.com', password: '23456' }),
            });
            expect(wrapper.vm.isFormCompleted).toBe(false);
          });
        });

        it('should be false when has only email', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'teste@teste.com', password: '' }),
          });
          expect(wrapper.vm.isFormCompleted).toBe(false);
        });

        it('should be false when has only password', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ password: '123456' }),
          });
          expect(wrapper.vm.isFormCompleted).toBe(false);
        });

        it('should be false when has (email and password) empty', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: '', password: '' }),
          });
          expect(wrapper.vm.isFormCompleted).toBe(false);
        });
      });
    });
  });

  describe('Validations', () => {
    describe('isValidEmail', () => {
      describe('When email is valid', () => {
        it('should return true', () => {
          const wrapper = shallowMount(Login);

          expect(wrapper.vm.isValidEmail('teste@teste.com')).toBe(true);
        });
      });
      describe('When email is NOT valid', () => {
        it('should return false', () => {
          const wrapper = shallowMount(Login);

          expect(wrapper.vm.isValidEmail('teste')).toBe(false);
        });
      });
    });

    describe('isValidPassword', () => {
      describe('When password is valid', () => {
        it('should return true', () => {
          const wrapper = shallowMount(Login);

          expect(wrapper.vm.isValidPassword('abc123')).toBe(true);
        });
      });
      describe('When password is NOT valid', () => {
        it('should return false', () => {
          const wrapper = shallowMount(Login);

          expect(wrapper.vm.isValidPassword('e')).toBe(false);
        });
      });
    });

    describe('Error messages', () => {
      describe('When password is less than 6 characters', () => {
        it('should show an error message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'teste@teste.com', password: '1' }),
          });
          expect(wrapper.find('.form__password_error').text()).toBe('Senha não pode ser menor que 6 caracteres');
          expect(wrapper.find('.form__password_error').attributes('style')).not.toBe('display: none;');
        });
      });
    });

    describe('When email and password match as valid to sign in', () => {
      it('should go to UserForm', async () => {
        const $router = {
          push: jest.fn(),
        };
        const emailPayload = 'email_100@gmail.com';
        const wrapper = shallowMount(Login, {
          data: () => ({ email: emailPayload, password: 'passord100' }),
          mocks: {
            $router,
          },
        });

        wrapper.find('.form__button_do_login').trigger('click');

        await wrapper.vm.$forceUpdate();

        expect(wrapper.vm.$router.push).toHaveBeenCalledWith({ name: 'UserForm', params: { user_email: emailPayload } });
      });
    });

    describe('When email and password do not match as valid to sign in', () => {
      it('should display an error message', async () => {
        const wrapper = shallowMount(Login, {
          data: () => ({ email: 'email_100@gmail.scom', password: 'passord100' }),
        });

        wrapper.find('.form__button_do_login').trigger('click');

        await wrapper.vm.$forceUpdate();

        expect(wrapper.find('.form__login_error').text()).toBe('Email/senha não encontrado');
        expect(wrapper.find('.form__login_error').attributes('style')).not.toBe('display: none;');
      });
    });
  });
});
